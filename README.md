# Color Namer
Some ~~impractical~~ **elegant** color names for HTML

[Try it out!](http://g-dv.gitlab.io/color-namer)

![Demo](public/demo.gif)

## What is this?

The idea comes from this post on StackOverflow: [Why does HTML think “chucknorris” is a color?](https://stackoverflow.com/questions/8318911/why-does-html-think-chucknorris-is-a-color).

Indeed, writing `<td bgcolor="chucknorris"></td>` gives a red cell. This is because the html interpreter tries to convert "chucknorris" into a valid hex value. Therefore, it replaces invalid characters with zeros, and then splits the word into three groups. The resulting hex color is "#c00000".

Using this principle, for a given color, we can try to find a word which results in a similar color. This is what this website does.

## Limitation

Sadly, since English words are not made of integers, many areas of the 3D color space cannot be described in HTML with a word.

If a color's hexadecimal representation requires an integer > 0, then we cannot represent it using an English word.

**Example**:
When `0x0F < R < 0xA0`, the hexadecimal representation of _R_ must have a strictly positive integer.
Therefore, we cannot find any word for colors that have: `15 < R < 160`.

### Visualization

The image below shows that there are major gaps where we can't find words.

![Accessible areas](public/words-in-color-space.png)

## Details

**How are colors compared?**

_Using [deltaE](https://en.wikipedia.org/wiki/Color_difference) with [CIE94](https://en.wikipedia.org/wiki/Color_difference#CIE94)._

**Where do the words come from?**

_`/usr/share/dict/american-english`_
