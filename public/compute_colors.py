"""
This script is used to generate words.js file
"""

import json

def get_hex(word):
    # replace unauthorized letters by zeros
    to_zero = lambda x: x if (x in "abcdef") else "0"
    word = "".join([to_zero(e) for e in word.lower()])
    # make the word's length a multiple of 3
    while len(word) % 3:
        word += "0"
    # split in 3 groups
    n = len(word) // 3
    r, g, b = word[:n], word[n : 2 * n], word[2 * n :]
    # make sure each group is 2 characters
    trim = lambda x: (("0" if len(x) < 2 else "") + x)[:2]
    # return the hex
    return f"#{trim(r)}{trim(g)}{trim(b)}"

# create the dict
with open("words.txt", "r") as file:
    words = {}
    for word in file.read().splitlines():
        color = get_hex(word)
        if color not in words or len(word) < len(words[color]):
            words[color] = word

# write it to valid javascript
with open("words.js", "w") as file:
    file.write(f"const words = {words};")
