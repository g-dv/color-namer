/**
 * Controller
 * Updates the html when the user picks a color
 */
window.onload = () => {
    colorPicker = document.getElementById("color-picker");
    colorPicker.onchange = () => {
        let minDiff = Infinity;
        let similarHex = null;
        let similarColor = null;
        for (const key in words) {
            let diff = deltaE(key, colorPicker.value);
            if (diff < minDiff) {
                minDiff = diff;
                similarHex = key;
                similarColor = words[key];
            }
        }
        document.getElementById("selected-color-hex").innerText = colorPicker.value;
        document.getElementById("similar-color-name").innerText = similarColor;
        document.getElementById("similar-color-hex").innerText = similarHex;
        document.getElementById("similar-color-example").value = similarHex;
    };
};

/**
 * Returns the difference between two colors
 * Inspired directly by the work of: https://github.com/antimatter15/rgb-lab
 * @param {*} hexA first color eg. #00ccff
 * @param {*} hexB second color eg. #00ffcc
 * @return distance between both colors
 */
function deltaE(hexA, hexB) {
    let labA = hex2lab(hexA);
    let labB = hex2lab(hexB);
    let deltaL = labA[0] - labB[0];
    let deltaA = labA[1] - labB[1];
    let deltaB = labA[2] - labB[2];
    let c1 = Math.sqrt(labA[1] * labA[1] + labA[2] * labA[2]);
    let c2 = Math.sqrt(labB[1] * labB[1] + labB[2] * labB[2]);
    let deltaC = c1 - c2;
    let deltaH = deltaA * deltaA + deltaB * deltaB - deltaC * deltaC;
    deltaH = deltaH < 0 ? 0 : Math.sqrt(deltaH);
    let sc = 1.0 + 0.045 * c1;
    let sh = 1.0 + 0.015 * c1;
    let deltaLKlsl = deltaL / (1.0);
    let deltaCkcsc = deltaC / (sc);
    let deltaHkhsh = deltaH / (sh);
    let i = deltaLKlsl * deltaLKlsl + deltaCkcsc * deltaCkcsc + deltaHkhsh * deltaHkhsh;
    return i < 0 ? 0 : Math.sqrt(i);
}

/**
 * Converts the color in hexadecimal RGB to LAB CIE94 color space
 * Inspired directly by the work of: https://github.com/antimatter15/rgb-lab
 * @param {*} hex color eg. #00ccff
 * @return color in LAB. eg. [0, 10, 20]
 */
function hex2lab(hex) {
    let rgb = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    let r = parseInt(rgb[1], 16) / 255;
    let g = parseInt(rgb[2], 16) / 255;
    let b = parseInt(rgb[3], 16) / 255;
    let x, y, z;
    r = (r > 0.04045) ? Math.pow((r + 0.055) / 1.055, 2.4) : r / 12.92;
    g = (g > 0.04045) ? Math.pow((g + 0.055) / 1.055, 2.4) : g / 12.92;
    b = (b > 0.04045) ? Math.pow((b + 0.055) / 1.055, 2.4) : b / 12.92;
    x = (r * 0.4124 + g * 0.3576 + b * 0.1805) / 0.95047;
    y = (r * 0.2126 + g * 0.7152 + b * 0.0722) / 1.00000;
    z = (r * 0.0193 + g * 0.1192 + b * 0.9505) / 1.08883;
    x = (x > 0.008856) ? Math.pow(x, 1 / 3) : (7.787 * x) + 16 / 116;
    y = (y > 0.008856) ? Math.pow(y, 1 / 3) : (7.787 * y) + 16 / 116;
    z = (z > 0.008856) ? Math.pow(z, 1 / 3) : (7.787 * z) + 16 / 116;
    return [(116 * y) - 16, 500 * (x - y), 200 * (y - z)]
}
