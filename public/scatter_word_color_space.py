"""
This script is used to generate the 3D plot for "words-in-color.png"
"""

import json
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

with open("words.js", "r") as file:
    # read the colors associated with English words
    content = file.read()[14:-1].replace("'", "\"")
    colors = list(json.loads(content).keys())

    # make a list of r, g, b values between 0 and 255
    r, g, b = [[(int(color[1:], 16) >> shift) % 256 for color in colors] for shift in [16, 8, 0]]

    # 3D scatter plot
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(len(r)):
        ax.scatter(r[i], g[i], b[i], color=colors[i], marker=".")
    ax.set_xlabel('R')
    ax.set_ylabel('G')
    ax.set_zlabel('B')
    plt.title("Color of English words")
    plt.show()
